import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Hi VAGILE-TECH TEAM</h1>
    <p>welcome to GITLAB DEMO</p>
    <p>welcome to GITLAB demo1</p>
    <p>welcome to GITLAB demo2</p>
    <p>welcome to GITLAB demo3</p>
    <p>learning gitlab is easy</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">Go to page 2</Link> <br />
    <div>Version: %%version%%</div>
    <Link to="/using-typescript/">Go to "Using TypeScript"</Link>
  </Layout>
)

export default IndexPage
